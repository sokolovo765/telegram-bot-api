from telebot.types import Message, CallbackQuery
from states.user_information import UserInfoState
from keyboards.inline.keyboard_city_choice import city_markup
from loader import bot
from handlers.custom_handlers.dictionars import comands_dict


@bot.message_handler(commands=['lowprice', 'highprice'])
def get_command(message: Message) -> None:
    comands_dict['command'] = message.text
    bot.send_message(message.from_user.id, 'Укажите город')
    bot.register_next_step_handler(message, city)


def city(message):
    bot.send_message(message.from_user.id, 'Уточните, пожалуйста:', reply_markup=city_markup(message.text))


@bot.callback_query_handler(func=lambda call: city_markup)
def get_city(call: CallbackQuery) -> None:
    bot.send_message(call.from_user.id, 'показать фото отелей ? (y/n)')
    bot.set_state(call.from_user.id, UserInfoState.need_foto)
    with bot.retrieve_data(call.from_user.id, call.message.chat.id) as data:
        data['city_id'] = call.data


