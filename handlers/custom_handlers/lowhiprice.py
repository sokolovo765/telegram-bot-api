import datetime
from loader import bot
from telebot.types import Message, CallbackQuery, ReplyKeyboardRemove, InputMediaPhoto
from states.user_information import UserInfoState
from keyboards.inline.keyboard_calendar import Calendar, CallbackData, RUSSIAN_LANGUAGE
from handlers.custom_handlers.scrapping_API import get_response_hotels, get_foto
from handlers.custom_handlers.dictionars import comands_dict, user_choice_calendar, dict_states

calendar = Calendar(language=RUSSIAN_LANGUAGE)
calendar_1_callback = CallbackData("calendar_1", "action", "year", "month", "day")


def selected(hotels_list, call):
    picture_list = get_pic(hotels_list)
    print(hotels_list)
    print(picture_list)
    for i_count in range(int(dict_states['quan_hotels'])):
        if UserInfoState.need_foto:
            bot.send_media_group(call.from_user.id, handler_pic_list(picture_list, i_count))
            bot.send_message(call.from_user.id,
                             text=answer_info_about_hotel(answer_list=hotels_list, count=i_count))
        else:
            bot.send_message(call.from_user.id,
                             text=answer_info_about_hotel(answer_list=hotels_list, count=i_count))
    picture_list.clear()
    hotels_list.clear()


def get_hotels_list(response):
    temp_list = response
    return temp_list


def get_pic(hot_list):
    pic_list = []
    for i_dict in hot_list:
        for i_key, val in i_dict.items():
            if i_key == 'id':
                temp_list = get_foto(hotel_id=val, count=dict_states['quan_foto'])
                pic_list.append(temp_list)
    return pic_list


def handler_pic_list(pic_l, count):
    link_pic = []
    for i in pic_l[count]:
        link_pic.append(InputMediaPhoto(i))
    return link_pic


def answer_info_about_hotel(answer_list, count):
    info_str = ''
    for i, v in answer_list[count].items():
        info_str += ' ' + str(i) + ' : ' + str(v) + '\n'
    info_str += '\n'

    return info_str


@bot.message_handler(state=UserInfoState.need_foto)
def your_need_foto(message: Message) -> None:
    if message.text == 'y' or 'Y':
        bot.send_message(message.from_user.id, 'Введите кол-во фото для вывода не более 5')
        bot.set_state(message.from_user.id, UserInfoState.quan_foto)
        with bot.retrieve_data(message.from_user.id, message.chat.id) as data:
            data['need_foto'] = True

    elif message.text == "n" or 'N':
        bot.send_message(message.from_user.id, 'Введите количество отелей для поиска ( не более 5 )')
        bot.set_state(message.from_user.id, UserInfoState.quan_hotels, message.chat.id)
        with bot.retrieve_data(message.from_user.id, message.chat.id) as data:
            data['need_foto'] = False

    else:
        bot.send_message(message.from_user.id, 'Введите корректный ответ')


@bot.message_handler(state=UserInfoState.quan_foto)
def quan_foto(message: Message) -> None:
    if message.text.isdigit() and int(message.text) <= 5:
        bot.send_message(message.from_user.id, 'Введите количество отелей для поиска ( не более 5 )')
        bot.set_state(message.from_user.id, UserInfoState.quan_hotels)
        with bot.retrieve_data(message.from_user.id, message.chat.id) as data:
            data['quan_foto'] = message.text
    else:
        bot.send_message(message.from_user.id, 'Ответ может содержать только число и не более 5')


@bot.message_handler(state=UserInfoState.quan_hotels)
def get_quan_hotels(message: Message) -> None:
    if message.text.isdigit() and int(message.text) <= 5:
        now = datetime.datetime.now()
        bot.send_message(
            message.chat.id,
            "Выберите дату заезда",
            reply_markup=calendar.create_calendar(
                name=calendar_1_callback.prefix,
                year=now.year,
                month=now.month)
        )
        with bot.retrieve_data(message.from_user.id, message.chat.id) as data:
            data['quan_hotels'] = message.text

    else:
        bot.send_message(message.from_user.id, 'Введите корректное значение')


@bot.callback_query_handler(func=lambda call: call.data.startswith(calendar_1_callback.prefix))
def callback_inline(call: CallbackQuery):
    name, action, year, month, day = call.data.split(calendar_1_callback.sep)
    date = calendar.calendar_query_handler(
        bot=bot, call=call, name=name, action=action, year=year, month=month, day=day)

    if action == "DAY":
        if 'arrive' not in user_choice_calendar:
            text = f"{date.strftime('%Y-%m-%d')}"
            with bot.retrieve_data(call.from_user.id, call.message.chat.id) as data:
                data['date_arrive'] = text
            user_choice_calendar['arrive'] = True
            now = datetime.datetime.now()
            bot.send_message(
                chat_id=call.from_user.id,
                text='Идет поиск подождите .....',
                reply_markup=ReplyKeyboardRemove())

            bot.send_message(
                chat_id=call.from_user.id,
                text="Выберите дату выезда",
                reply_markup=calendar.create_calendar(
                    name=calendar_1_callback.prefix,
                    year=now.year,
                    month=now.month)
            )

        elif 'depart' not in user_choice_calendar:
            text = f"{date.strftime('%Y-%m-%d')}"
            with bot.retrieve_data(call.from_user.id, call.message.chat.id) as data:
                data['date_depart'] = text
                for i_data, v_data in data.items():
                    dict_states[i_data] = v_data
            user_choice_calendar['depart'] = True
            print(comands_dict)
            if comands_dict['command'] == '/lowprice':
                hotels_list = get_hotels_list(get_response_hotels(city_id=dict_states['city_id'],
                                                                  page=dict_states['quan_hotels'],
                                                                  chin=dict_states['date_arrive'],
                                                                  chout=dict_states['date_depart']))
                selected(hotels_list, call)
            elif comands_dict['command'] == '/highprice':
                hotels_list = get_hotels_list(get_response_hotels(city_id=dict_states['city_id'],
                                                                  page=dict_states['quan_hotels'],
                                                                  chin=dict_states['date_arrive'],
                                                                  chout=dict_states['date_depart'],
                                                                  val_price="PRICE_HIGHEST_FIRST"))
                selected(hotels_list, call)

            bot.delete_state(call.from_user.id, call.message.chat.id)
            user_choice_calendar.clear()
            dict_states.clear()

    elif action == "CANCEL":
        bot.send_message(
            chat_id=call.from_user.id,
            text="Cancellation",
            reply_markup=ReplyKeyboardRemove(),
        )
        print(f"{calendar_1_callback}: Cancellation")
