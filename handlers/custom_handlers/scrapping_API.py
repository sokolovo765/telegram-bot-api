from config_data.config import RAPID_API_KEY
import requests

filter_hotel = []


def get_city_list(city):
    filtred_city_list = []

    url = "https://hotels4.p.rapidapi.com/locations/v2/search"
    querystring = {"query": city, "locale": "en_US", "currency": "USD"}
    headers = {
        "X-RapidAPI-Key": RAPID_API_KEY,
        "X-RapidAPI-Host": "hotels4.p.rapidapi.com"
    }

    response = requests.request("GET", url, headers=headers, params=querystring)
    data = response.json()
    suggestions_list = data.get("suggestions")
    city_group = suggestions_list[0]['entities']

    for i_dict in city_group:
        filtred_city_list.append({'name': i_dict.get('name'), 'loc': i_dict.get("destinationId")})

    return filtred_city_list


def get_response_hotels(city_id, page, chin, chout, val_price="PRICE"):
    url = "https://hotels4.p.rapidapi.com/properties/list"
    querystring = {"destinationId": city_id, "pageNumber": "1", "pageSize": page,
                   "checkIn": chin,
                   "checkOut": chout,
                   "adults1": "1", "sortOrder": val_price, "locale": "en_US",
                   "currency": "USD"}
    headers = {
        "X-RapidAPI-Key": RAPID_API_KEY,
        "X-RapidAPI-Host": "hotels4.p.rapidapi.com"
    }

    response = requests.request("GET", url, headers=headers, params=querystring)
    temp_data = response.json()
    data_dict = temp_data.get('data')
    result = data_dict['body']['searchResults']['results']

    for i_dict in result:
        filter_hotel.append({'id': i_dict.get('id'), 'name': i_dict.get('name'),
                             'address': i_dict['address'].get('streetAddress'),
                             'distance to center': i_dict.get('landmarks')[0].get('distance'),
                             'price': i_dict['ratePlan']['price'].get('current'),
                             'total_cost': i_dict['ratePlan']['price'].get('fullyBundledPricePerStay')
                            .lstrip('total').rstrip('&nbsp;nights')})

    return filter_hotel


def get_foto(hotel_id, count):
    url = "https://hotels4.p.rapidapi.com/properties/get-hotel-photos"
    querystring = {"id": hotel_id}
    headers = {
        "X-RapidAPI-Key": RAPID_API_KEY,
        "X-RapidAPI-Host": "hotels4.p.rapidapi.com"
    }

    response = requests.request("GET", url, headers=headers, params=querystring)
    pic_list = []

    data = response.json()
    for i_pic in range(int(count)):
        pic_list.append(data['hotelImages'][i_pic]['baseUrl'].replace('{size}', 'w'))

    return pic_list

