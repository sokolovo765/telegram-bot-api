from telebot.types import InlineKeyboardButton, InlineKeyboardMarkup
from handlers.custom_handlers.scrapping_API import get_city_list


def city_markup(city_name):
    cities = get_city_list(city_name)
    destinations = InlineKeyboardMarkup()
    for i_city in cities:
        destinations.add(InlineKeyboardButton(text=i_city['name'],
                                              callback_data=f'{i_city["loc"]}'))
    return destinations
