from telebot.handler_backends import State, StatesGroup


class UserInfoState(StatesGroup):
    date_depart = State()
    date_arrive = State()
    quan_hotels = State()
    city_id = State()
    quan_foto = State()
    need_foto = State()



